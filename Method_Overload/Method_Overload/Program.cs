﻿using System;

namespace Method_Overload
{
     public class A
        {
            public double Area(double s)
            {
                double area = s * s;
                return area;
            }

            public double Area(double l, double b)
            {
                double area = l * b;
                return area;
            }
        }

    class Cal
    {
        public static void Main(string[] args)
            {
                A a = new A();
                double length = 3.3;
                double breadth = 4.4;
                double rect = a.Area(length, breadth);
                Console.WriteLine("Area of rectangle " + rect);

                double side = 3.3;
                double square = a.Area(side);
                Console.WriteLine(square);
            }
    }

}
