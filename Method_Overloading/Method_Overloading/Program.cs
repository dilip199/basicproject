﻿using System;

namespace Method_Overloading
{
    public class Cal
    {
        public static int add(int a, int b)
        {
            return a + b;
        }
        public static float add(float a, float b)
        {
            return a + b;
        }
        public static double add(double a,double b)
        {
            return a + b;
        }

    }
    public class methodoverloading
    {
      public static void Main()
      {
            Console.WriteLine(Cal.add(10, 20));
            Console.WriteLine(Cal.add(10.4f, 20.3f));
            Console.WriteLine(Cal.add(12.4,12.4));

        }
    }
 }

