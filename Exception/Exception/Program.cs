﻿using System;

namespace Exception
{
    class Div_Numbers
    {
        int result;

    Div_Numbers()
    {
        result = 0;
    }
    public void Division(int num1, int num2)
    {
        try
        {
            result = num1 / num2;
        }
        catch (DivideByZeroException e)
        {
            Console.WriteLine("Exception caught: {0}", e);
        }
        finally
        {
            Console.WriteLine("Result: {0}", result);
            }
    }
    static void Main(string[] args)
    {
        Div_Numbers d = new Div_Numbers();
        d.Division(25, 0);
        Console.ReadKey();
    }
}
}
