﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Wizlib_DataAccess.Migrations
{
    public partial class updateAuthorTablePrimaryKeyName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Authors_Id",
                table: "Authors",
                newName: "Author_Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Author_Id",
                table: "Authors",
                newName: "Authors_Id");
        }
    }
}
