﻿using System;
using Microsoft.EntityFrameworkCore;
using Wizlib_Model.Models;

namespace Wizlib_DataAccess.Data
{
    public class ApplicationDbcontext : DbContext
    {
        public ApplicationDbcontext(DbContextOptions<ApplicationDbcontext> options) : base(options)
        {
        }

        //public DbSet<Category> Categories { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Publisher> Publishers { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<BookAuthor> BookAuthors { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BookAuthor>().HasKey(ba => new { ba.Author_Id, ba.Book_Id });

            modelBuilder.Entity<BookDetail>().HasKey(b =>b.BookDetail_Id );

            modelBuilder.Entity<BookDetail>().Property(b => b.NumberOfChapters).IsRequired();

        }
    }
}
