﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Wizlib_Model.Models
{
    public class Fluent_Publisher
    {
       
        public int Publisher_Id { get; set; }

       
        public string Name { get; set; }

       
        public string Location { get; set; }


    }
}
