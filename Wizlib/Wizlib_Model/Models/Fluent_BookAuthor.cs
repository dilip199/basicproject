﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Wizlib_Model.Models
{
    public class Fluent_BookAuthor
    {
        

        public Book Book { get; set; }

        public Author Author { get; set; }
    }
}
