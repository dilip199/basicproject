﻿using System;

namespace Loop
{
    class Program
    {
        public static void Main(string[] args)
        {
            {

               for (int i = 0; i < 5; i++)
                {
                    if (i % 2 == 0)
                    {
                        Console.WriteLine("Even");
                    }
                    else
                    {
                        Console.WriteLine("Not Even");
                    }
                }

                Console.WriteLine("------------");

                int j = 1;
                while (j < 5)
                {
                    if (j % 3 == 0)
                    {
                        Console.WriteLine("odd");
                    }
                    else
                    {
                        Console.WriteLine("Not odd");
                    }
                    j++;
                }

                Console.WriteLine("------------");

                int k = 1;
                do
                {
                    if (k % 3 == 0)
                    {
                        Console.WriteLine("odd");
                    }
                    else
                    {
                        Console.WriteLine("Not odd");
                    }

                    k++;
                }
                while (k < 5);

                Console.WriteLine("------------");

                /* int[] a = new int[] { 1, 2, 3, 4, 5, 6, 7 };

                 foreach (int i in a)
                 {
                     Console.WriteLine(i);
                 }*/


            }
        }
    }
}