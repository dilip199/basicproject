﻿using System;

namespace Aggregation
{
    public class Address
    {
        public string address, city, state;
        public Address(string address, string city, string state)
        {
            this.address = address;
            this.city = city;
            this.state = state;
        }
    }
    public class Employee
    {
        public int id;
        public string name;
        public Address address;
        public Employee(int id, string name, Address address)
        {
            this.id = id;
            this.name = name;
            this.address = address;
        }
        public void display()
        {
            Console.WriteLine(id + " " + name + " " +
              address.address + " " + address.city + " " + address.state);
        }
    }
    public class Agg
    {
        static void Main(string[] args)
        {
            Address a = new Address("Mogappair", "Chennai", "TamilNadu");
            Employee e = new Employee(1, "Dilip", a);
            e.display();
        }
    }
}
