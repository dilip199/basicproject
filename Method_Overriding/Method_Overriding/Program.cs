﻿using System;

namespace Method_Overriding
{
    class Overr
    {
        public class A
        {
            public virtual void a1()
            {
                Console.WriteLine("class A");
            }
        }
        public class B : A
        {
            public override void a1()
            {
                Console.WriteLine("class B");
            }
        }

        static void Main(string[] args)
        {
            B n = new B();
            n.a1();
        }
    }
}
