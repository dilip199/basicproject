﻿using System;
using MySql.Data.MySqlClient;

namespace MySQLConnect
{
    class Program
    {
        static MySqlConnection con;

        public static void Connect(string user, string password)
        {
            con = new MySqlConnection();


            try
            {
                con.ConnectionString = "server = localhost; User Id = " + user + "; " +
                    "Persist Security Info = True; database = sys; Password = " + password;
                con.Open();
                //insertquer();
                //updatequer();
                deletequer();
                Console.WriteLine("Succesfully connected!");

            }

            catch (Exception e)
            {
                Console.WriteLine("Not Successful! due to " + e.ToString());
            }
        }

        // public static void insertquer()
        //{
        //    MySqlCommand command;
        //    MySqlDataAdapter adapter = new MySqlDataAdapter();
        //    string sql = "";

        //    sql = "Insert into Employee(Id,Name,Age,DateOfBirth,Phone) value(2,'kumar',20,'14-06-1999','9856789032')";

        //    command = new MySqlCommand(sql, con);
        //    adapter.InsertCommand = new MySqlCommand(sql, con);
        //    adapter.InsertCommand.ExecuteNonQuery();

        //    command.Dispose();
        //}

        //public static void updatequer()
        //{
        //    MySqlCommand command;
        //    MySqlDataAdapter adapter = new MySqlDataAdapter();
        //    string sql = "";

        //    sql = "Update Employee set Name='raja' where Id=5";

        //    command = new MySqlCommand(sql, con);
        //    adapter.InsertCommand = new MySqlCommand(sql, con);
        //    adapter.InsertCommand.ExecuteNonQuery();

        //    command.Dispose();
        //}


        public static void deletequer()
        {
            MySqlCommand command;
            MySqlDataAdapter adapter = new MySqlDataAdapter();
            string sql = "";

            sql = "Delete from Employee where Id=5";

            command = new MySqlCommand(sql, con);
            adapter.DeleteCommand = new MySqlCommand(sql, con);
            adapter.DeleteCommand.ExecuteNonQuery();

            command.Dispose(); 
        }

        static void Main(string[] args)
        {
            Connect("root", "10decoders");
            con.Close();
            Console.WriteLine("Hello World!");
        }
    }
}
