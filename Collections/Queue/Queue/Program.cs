﻿using System;
using System.Collections.Generic;

namespace Queue
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue<string> n = new Queue<string>();

            n.Enqueue("dilip");

            n.Enqueue("kumar");

            n.Enqueue("raja");

            n.Enqueue("rajesh");


            foreach (string n1 in n)
            {
                Console.WriteLine(n1);
            }

            Console.WriteLine("Peek element: " + n.Peek());

            Console.WriteLine("Dequeue " + n.Dequeue());

            Console.WriteLine("After Dequeue, Peek element: " + n.Peek());

        }
    }
}
