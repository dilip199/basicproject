﻿using System;
using System.Collections.Generic;

namespace SortedList
{
    class Program
    {
        static void Main(string[] args)
        {
            SortedList<string, string> names = new SortedList<string, string>();

            names.Add("1", "dilip");

            names.Add("4", "kumar");

            names.Add("5", "rajesh");

            names.Add("3", "rakesh");

            names.Add("2", "abdul");

            foreach (KeyValuePair<string, string> kv in names)
            {
                Console.WriteLine(kv.Key + " " + kv.Value);
            }
        }
    }
}
