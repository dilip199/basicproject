﻿using System;
using System.Collections.Generic;

namespace Linkedlist
{
    class Program
    {

        static void Main(string[] args)
        {
            var n = new LinkedList<string>();

            n.AddLast("kamal");

            n.AddLast("rajesh");

            n.AddLast("raja");

            n.AddLast("kumar");

            n.AddFirst("dilip");


            LinkedListNode<String> n2 = n.Find("kumar");

            n.AddBefore(n2, "John");

            n.AddAfter(n2, "Lucy");

            foreach (var n1 in n)
            {
                Console.WriteLine(n1);
            }
        }
    }
}

