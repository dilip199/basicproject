﻿using System;
using System.Collections.Generic;

namespace Sorted_Dict
{
    class Program
    {
        static void Main(string[] args)
        {
            SortedDictionary<string, string> names = new SortedDictionary<string, string>();

            names.Add("1", "kumar");

            names.Add("4", "dilip");

            names.Add("5", "rajesh");

            names.Add("3", "kamal");

            names.Add("2", "orton");

            foreach (KeyValuePair<string, string> kv in names)
            {
                Console.WriteLine(kv.Key + " " + kv.Value);
            }

        }
    }
}
