﻿using System;
using System.Collections.Generic;

namespace List
{
    class Program
    {
        public static void Main(string[] args)
        {
            var names = new List<string>();

            names.Add("dilip");

            names.Add("kumar");

            names.Add("raja");

            names.Add(null);

            foreach (var n in names)
            {
                Console.WriteLine(n);
            }

            var n2 = new List<string>()
            {
                "dilip",
                "kumar",
                "raja",
                "erwin"
            };

            foreach (var n1 in n2)
            {
                Console.WriteLine(n1);
            }

        }
    }
}
