﻿using System;
using System.Collections.Generic;

namespace Dictionary
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, string> names = new Dictionary<string, string>();

            names.Add("1", "dilip");

            names.Add("2", "kumar");

            names.Add("3", "raja");

            names.Add("4", "rajesh");

            names.Add("5", "kamal");

            foreach (KeyValuePair<string, string> kv in names)
            {
                Console.WriteLine(kv.Key + " " + kv.Value);
            }
        }
    }
}
