﻿using System;

namespace Generic
{
    class Geneclass<T>
    {
        public Geneclass(T a)
        {
            Console.WriteLine(a);
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            Geneclass<string> g1 = new Geneclass<string>("generic class");
            Geneclass<int> g2 = new Geneclass<int>(1);
            Geneclass<char> g3 = new Geneclass<char>('I');
        }
    }
}
